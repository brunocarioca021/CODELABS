//Blue EdTech - Prof. Alexandre Russi 
//Aluno: Bruno Dutra
//DESAFIO CODELAB 23/09/2021 

///DESAFIO 3
// Caixa eletrônico
// Faça um Programa para um caixa eletrônico. O programa deverá perguntar ao usuário a valor do saque e depois informar quantas notas de cada valor serão fornecidas. As notas disponíveis serão as de 1, 5, 10, 50 e 100 reais. O valor mínimo é de 10 reais e o máximo de 600 reais. O programa não deve se preocupar com a quantidade de notas existentes na máquina.
// Exemplo 1: Para sacar a quantia de 256 reais, o programa fornece duas notas de 100, uma nota de 50, uma nota de 5 e uma nota de 1;
// Exemplo 2: Para sacar a quantia de 399 reais, o programa fornece três notas de 100, uma nota de 50, quatro notas de 10, uma nota de 5 e quatro notas de 1.

console.log("***** CAIXA ELETRÔNICO *****");
console.log();
console.log(`Notas disponíveis para saque
 R$1,00 
 R$5,00 
 R$10,00 
 R$50,00 
 R$100,00
 `);
let saque = +prompt("Valor para o saque [valores: 10-1000]: ");
console.log();

notaCem = Math.trunc(saque /100);
saque = saque - (notaCem * 100);

notaCinquenta = Math.trunc(saque /50);
saque = saque - (notaCinquenta * 50);

notaDez = Math.trunc(saque /10);
saque = saque - (notaDez * 10);

notaCinco = Math.trunc(saque /5);
saque = saque - (notaCinco * 5);

notaUm = saque;

console.log(`Notas de R$100,00 = ${notaCem}`);
console.log(`Notas de R$50,00 = ${notaCinquenta}`);
console.log(`Notas de R$10,00 = ${notaDez}`);
console.log(`Notas de R$5,00 = ${notaCinco}`);
console.log(`Notas de R$1,00 = ${notaUm}`);
