//Blue EdTech - Prof. Alexandre Russi 
//Aluno: Bruno Dutra
//DESAFIO CODELAB 23/09/2021 

// DESAFIO 01
// Reajuste salarial
// As empresas @.com resolveram dar um aumento de salário aos seus colaboradores e lhe contrataram para desenvolver o programa que calculará os reajustes.
// Faça um programa que recebe o salário de um colaborador e o reajuste segundo o seguinte critério, baseado no salário atual:
// •	salários até R$ 280,00 (incluindo) : aumento de 20%
// •	salários entre R\$ 280,00 e R$ 700,00 : aumento de 15%
// •	salários entre R\$ 700,00 e R$ 1500,00 : aumento de 10%
// •	salários de R$ 1500,00 em diante : aumento de 5%
// Após o aumento ser realizado, informe na tela:
// •	o salário antes do reajuste;
// •	o percentual de aumento aplicado;
// •	o valor do aumento;
// •	o novo salário, após o aumento.

let salario = +prompt("Digite o salário do colaborador: ");

if (salario <= 280) 
{
porcentagem = 20;
} 
else if (salario <= 700) 
{
porcentagem = 15;
} 
else if (salario <= 1500) 
{
porcentagem = 10;
} 
else 
{
porcentagem = 5;
}

console.log(`Salário é: R$ ${salario} `);
console.log();
console.log(`Valor do percentual: ${porcentagem}%`);
console.log();

porcentagem = porcentagem / 100;
novoAumento = porcentagem * salario;
salarioAjustado = salario + novoAumento;

console.log(`O novo reajuste salarial foi de: R$ ${novoAumento.toFixed(2)}`);
console.log();
console.log(`Salário como novo valor de R$ ${salarioAjustado.toFixed(2)}`);



