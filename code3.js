//Blue EdTech - Prof. Alexandre Russi 
//Aluno: Bruno Dutra
//DESAFIO CODELAB DATA 28/09/21

//AULA 07 – CODELAB – EXERCÍCIOS PARA TREINO

//Exercício 1 - Calcular a soma de todos os dígitos de um número inteiro positivo.

// function numerosInteiros(digito) 
// {
//   const digitoString = digito.toString();

//   let soma = 0;
  
//   for (const char of digitoString) 
//   {
//     const digito = +char;
//   }
//   return soma;
// }

// const resultado = numerosInteiros(1235231);
// console.log(`Soma de todos os dígitos: ${resultado}.`);


//Exercício 2 - Criar uma função que junte dois arrays e retorne o resultado como um novo array.

// function juntaArrays(array1, array2) 
// {
//   let array = [];

//   for (let elemento of array1) 
//   {
//     array.push(elemento);
//   }

//   for (let elemento of array2) 
//   {
//     array.push(elemento);
//   }

//   return array;
// }

// let arrayNumber1 = [1, 2, 3];
// let arrayNumber2 = [4, 5, 6];

// const arrayJunto = juntaArrays(arrayNumber1, arrayNumber2);

// console.log(arrayJunto);

//Exercício 3 -  Contar a quantidade de palavras em um texto

function qtdPalavras(text) 
{
  let separar = true;
  let palavras = 0;

  for (var texts of text) 
  {
    if (separador(texts)) 
    {
      separar = true;

      continue;
    } 
    if (separar) 
    {
      palavras++;

      separar = false;
    }
  }

  return palavras;
}

function separador(texts) 
{
  let separadores = [" ", "\t", "\n", "\r", ",", ";", ".", "!", "?" ];

  return separadores.includes(texts);
}

console.log(qtdPalavras(""));
console.log(qtdPalavras("Eu uso Linux!! "));
console.log(qtdPalavras("Confidencialidade"));
console.log(qtdPalavras("Integridade"));
console.log(qtdPalavras("Disponibilidade"));
console.log(qtdPalavras("Vamos estudar Segurança da informção"));




