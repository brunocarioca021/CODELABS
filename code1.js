//AULA CODELAB

//EXERCÍCIO 1 - Frase na tela - Implemente um programa que escreve na tela a frase "O primeiro programa a gente nunca esquece!".

console.log("O primeiro programa a gente nunca esquece! ");
console.log();

//EXERCÍCIO 2 - Etiqueta - Elabore um programa que escreve seu nome completo na primeira linha, seu endereço na segunda e o CEP e telefone na terceira.

// const nomeCompleto = prompt("Digite o seu nome: ");
// const endereco = prompt("Digite seu endereço: ");
// const cep = prompt("Digite seu CEP: ");
// const telefone = prompt("Digite seu telefone: ");
// console.log();

// console.log(
//   `Nome ${nomeCompleto}
// Endereço ${endereco}
// CEP: ${cep}
// Contato: ${telefone}
  
// `);


//EXERCÍCIO 3 - Letra de música - Faça um programa que mostre na tela uma letra de música que você gosta (proibido letras do Justin Bieber).

// console.log(`Pra onde iremos nós?
// Só Tu tens a vida eterna
// Pra onde iremos nós?
// Só Tu tens a vida eterna
// Tu és o pão que desceu do céu
// Fonte de vida, Emanuel
// Com Tua glória sobre mim
// Enche-me de ti
// Até transbordar
// Eu nunca vou me saciar
// Enche-me de ti
// Até transbordar
// Tu és o pão que desceu do céu
// Fonte de vida, Emanuel`);


//EXERCÍCIO 4 - Tabela de notas - Você foi contratado ou contratada por uma escola para fazer o sistema de boletim dos alunos.Como primeiro passo, escreva um programa que produza a seguinte saída:
// ALUNO (A)  NOTA
// =========  ====
// ALINE           9.0
// MÁRIO          DEZ
// SÉRGIO        4.5
// SHIRLEY       7.0

// console.log("ALUNO (A)  NOTA ");
// console.log("=========  ==== ");
// console.log(
// ` ALINE     9.0
//  MÁRIO     DEZ
//  SÉRGIO    4.5
//  SHIRLEY   7.0
// `);

//EXERCÍCIO 5 - Faça um programa de cadastro de clientes que mostre um menu de opções e permita com que a pessoa escolha umas das opções, exibindo qual foi a opção escolhida.
// Cadastro de Clientes
// 0 – Fim
// 1 - Inclui
// 2 - Altera
// 3 - Exclui
// 4 – Consulta

// console.log(" == Cadastro de Clientes == ");

// console.log();

// const menu = +prompt(`
// Digite as opções

//    0 - Fim
//    1 - Inclui
//    2 - Altera
//    3 - Exclui
//    4 - Consulta

//    `)
//   console.log(menu);

//EXERCÍCIO 6 - Calculadora de Dano - Escreva um programa que receba dois valores digitados pelo usuário:
//• Quantidade de vida de um monstro (entre 10 e 50);
//• Valor do ataque do jogador por turno (entre 5 e 10);
//• Baseado nos valores digitados, exiba a quantidade de turnos que o jogador irá demorar para conseguir derrotar o monstro.
//Exemplo: O jogador irá derrotar o monstro em 8 turnos.

// console.log("== Bem Vindo a Calculadora de Dano ==");

// console.log();

// const vidaMostro = +prompt("Digite quantidade de vida de um mostro (entre 10 e 50) ");
// const jogadorAtaques = +prompt("Digite o valor do ataque do jogador por turno (entre 5 e 10) ");
// const turnosBonus = Math.ceil(vidaMostro / jogadorAtaques);
// console.log(`O Jogador irá precisa de ${turnosBonus} turnos para detonar o monstro.`);


//EXERCÍCIO 7 - E os 10% do garçom?
// • Defina uma variável para o valor de uma refeição que custou R$ 42,54;
// • Defina uma variável para o valor da taxa de serviço que é de 10%;
// • Defina uma variável que calcula o valor total da conta e exiba-o no console com essa formatação: R$ 9999.99.

let valorRefeicao = 42.54;
let taxaServico = 10;
let total = valorRefeicao + valorRefeicao * (taxaServico / 100);
console.log(`R$ ${total.toFixed(2)}`)





