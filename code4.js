//Blue EdTech - Prof. Alexandre Russi 
//Aluno: Bruno Dutra
//DESAFIO CODELAB DATA 30/09/21


//EXERCÍCIO 1- Imprimir números de 1 a 10.

// for (let i =1; i<= 10; i++) 
// {
//   console.log(i);
// }

//EXERCÍCIO 2 - Imprimir a tabuada do número 8.

// for (let i = 1; i <= 10; i++) 
// {
//   let tabuada = "8 x " + i + " = " + 8* i;
//   console.log(tabuada);
// }

//EXERCÍCIO 3 -  Imprimir todas as tabuadas do número 1 ao 10.

// let nemero = +prompt('Informe o número: ');

// function ListaTabuada(numero) 
// {
//   for (let i =1; i <= 10; i++) 
//   {
//     let linha = numero + " x " + i + " = " + numero + i;
//     console.log(linha);
//   }
// }
// for (let i = 1; i < 10; i++) 
// {
//   ListaTabuada(i);
//   console.log("");
// }

//EXERCÍCIO 4 -  Soma dos números de 1 a 10.

// let soma = 0;
// for (let i = 1; i <= 10; i++) 
// {
//   soma += i;
// }
// console.log(`Resultado da soma: ${soma}`);

//EXERCÍCIO 5 -  Calcular a soma dos números de um array

// function somaArray(array) 
// {
//   let soma = 0;
//   for (let i = 0; i < array.length; i++) 
//   {
//     soma += array[i];
//   }
//   return soma;
// }

// let array =[8,8,8];
// let soma = somaArray(array);
// console.log(`A soma do array é: ${soma}`);

//Exercício 6 - Calcular a média de todos os números de um array

// function mediaArray(array) 
// {
//   let number = array.length;
//   let soma = 0;
//   for (let i = 0; i < number; i++) 
//   {
//     soma += array[i];
//   }
//   return soma / number;
// }

// let array = [10,20,30];
// let media = mediaArray(array);
// console.log(`Média Array é: ${media}`);

//Exercício 7 - Criar uma função que receba como parâmetro um array de números e retorne um array contendo somente números positivos.

// function retornoPositivos(array) 
// {
//   let array2 = [];
//   for (let i = 0; i < array.length; i++) 
//   {
//     let elementos = array[i];
//     if (elementos >= 0) 
//     {
//       array2.push(elementos);
//     }
//   }
//   return array2;
// }

// let array = [-7, 10, -7, 14, -3, 60, 0, 1];
// let array2 = retornoPositivos(array);
// console.log(`Números Positivosdo array: ${array2}`);

//Exercicio 8 - Localizar o maior valor dentro de um array de números

// function LocalizarMaior(array) 
// {
//   let maximo = array[0];
//   for (let i = 0; i < array.length; i++) 
//   {
//     if (array[i] > maximo) 
//     {
//       maximo = array[i];
//     }
//   }
//   return maximo;
// }

// let array = [-2,-1,8,5,98,7];
// let maximo = LocalizarMaior(array);
// console.log("Maior número:", maximo);

//Exercicio 9 - A Sequência de Fibonacci foi descrita primeiramente para descrever o crescimento de
// uma população de coelhos. Os números descrevem o número de casais em uma
// população de coelhos depois de n meses. Ela funciona assim, você começa com 0 e 1, e
// então produz o próximo número de Fibonacci somando os dois anteriores para formar
// o próximo, 0+1=1,1+1=2,2+1=3...
// Ex: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181,...
// Monte um algoritmo que possua uma estrutura de repetição e exiba na tela os 10
// primeiros termos da sequência de Fibonacci.

// Assim, a fórmula a seguir define a sequência:
// Fn = Fn - 1 + Fn – 2

// let fn0 = 0;
// console.log(fn0);
// let fn1 =1;
// console.log(fn1);
// for (let i = 2; i < 9; i++) 
// {
//   let fi = fn1 + fn0;
//   console.log(fi);
//   fn0 = fn1;
//   fn1 = fi;
// }

//Exercicio 10 - Retornar a maior string de um array

// function maiorString(array) 
// {
//   let maior = "";
//   for (let i = 0; i < array.length; i++) 
//   {
//     if (array[i].length > maior.length) 
//     {
//       maior = array[i];
//     }
//   }
//   return maior; 
// }
// console.log(`A maior STRING é: ${maiorString(["Java", "PHP", "CSS", "C", "C++", "JavaScript", "Python"])}`);

//Exercicio 11 -  Criar uma função para inverter um array

// let meuArray = [1, 2, 3, 4, 5, 6];
// let meuArrayInvertido = meuArray.slice(0).reverse();
// console.log(meuArrayInvertido);

function invertArray(array) 
{
  let array2 = [];
  for (let i = array.length -1; i >=0; i--) 
  {
    array2.push(array[i]);
  }
  return array2
}

let array = [1, 2, 3, 4, 5, 6];
let array2 = invertArray(array);
console.log(`Array invertido: ${array2}`);
