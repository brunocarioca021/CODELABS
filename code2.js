//Blue EdTech - Prof. Alexandre Russi 
//Aluno: Bruno Dutra
//EXERCÍCIOS CODELAB 23/09/2021 

//EXERCÍCIO 1 - Considere a string A = "Os limites só existem se você os deixar existir.(goku)". Que fatia corresponde a (goku)?

// let frase = "Os Limites só existem se você os deixar.(goku)";
// console.log(frase.substring(40));

//EXERCÍCIO 02 - Escreva um programa que solicite uma frase ao usuário e escreva a frase toda em maiúscula e sem espaços em branco.

// let frase = "olá tudo bem";
// console.log(frase.toUpperCase().replace(" " ,""));


// EXERCÍCIO 03 - Elabore um programa que recebe o seu nome, endereço e hobby e mostra cada uma das informações da seguinte forma:
// // Nome -> Letra maiúscula
// // Endereço -> Letra minúscula
// // Hobby -> Primeira letra maiúscula
// // Exemplo Entrada:
// Nome: bruno fabri
// Endereço: Rua ABC
// Hobby: jogar cs
// Exemplo Saída:
// Nome: BRUNO FABRI
// Endereço: rua abc
// Hobby: Jogar cs


let nome = prompt("Digite seu nome: ");
let endereco = prompt("Digite seu endereço: ");
let hobby = prompt("Digite o seu Hobby: ");

console.log(nome.toUpperCase());
console.log(endereco.toLowerCase());
console.log(hobby[0].toUpperCase() + hobby.slice(1).toLowerCase());



// EXERCÍCIO 04 - Faça um programa que pergunte ao usuário um número e valide se o numero é par ou impar:
// Crie uma variável para receber o valor, com conversão para int
// Para um número ser par, a divisão dele por 2 tem que dar resto 0

let numero = +prompt("Digite o número: ");

if (numero % 2 === 0) 
{
console.log("Número par");
} 
else 
{
console.log("Número ímpar");
}

console.log(numero);

//EXERCÍCIO 05 - Parte 1
// Faça um script que peça um valor e mostre na tela se o valor é positivo ou negativo.

let valor = +prompt("Digite um valor: ");

if (valor > 0) 
{
console.log("Valor positivo");
} 
else 
{
console.log("Valor negativo");
}

//Parte 2
// Agora implemente a funcionalidade de não aceitar o número 0, no input.

let valor = +prompt("Digite um valor: ");

if (valor > 0) 
{
console.log("Valor positivo");
} 
else if (valor === 0) 
{
console.log("Número inválido, digite o número válido");
}
else 
{
console.log("Valor negativo");
}

//EXERCÍCIO 06 - Faça um programa que peça dois números, imprima o maior deles ou imprima "Numeros iguais" se os números forem iguais.

let numero1 = +prompt("Informe o primeiro número: ");
let numero2 = +prompt("Informe o segundo número: ");

if (numero1 > numero2) 
{
console.log(`O número ${numero1} é o maior`);
} 
else if (numero1 === numero2) 
{
console.log(`Os múmeros são iguais!`);
}
else 
{
console.log(`O número ${numero2} é o maior`);
}

///EXERCÍCIO 07 - Crie um programa que verifique se uma letra digitada é "F" ou "M". Conforme a letra escrever: F - Feminino, M - Masculino, caso escreva outra letra: Sexo Inválido.

const pergunta = prompt("Digite M ou F: ").toUpperCase();

if (pergunta === "M") 
{
console.log("Masculino");
} 
else if (pergunta === "F") 
{
console.log("Feminino");
} 
else 
{
console.log("Letra inválida, digite as letras corretas!");
}

///EXERCÍCIO 08 - Crie um programa em JavaScript que peça a nota do aluno, que deve ser um number entre 0.00 e 10.0
// •	Se a nota for menor que 6.0, deve exibir a nota F.
// •	Se a nota for de 6.0 até 7.0, deve exibir a nota D.
// •	Se a nota for entre 7.0 e 8.0, deve exibir a nota C.
// •	Se a nota for entre 8.0 e 9.0, deve exibir a nota B.
// Por fim, se for entre 9.0 e 10.0, deve exibir um belo de um A.

const notaAluno = +prompt("Nota do Aluno (0.0 - 10.00): ");

if (notaAluno >= 0 && notaAluno <= 10) 
{
if (notaAluno < 6) 
{
console.log(`Nota do aluno(a) é ${notaAluno} então é F`);
} 
else if (notaAluno < 7) 
{
console.log(`Nota do aluno(a) é ${notaAluno} então é D`);
} 
else if (notaAluno < 8) 
{
console.log(`Nota do aluno(a) é ${notaAluno} então é C`);
} 
else if (notaAluno < 9) 
{
console.log(`Nota do aluno(a) é ${notaAluno} então é B`);
} 
else if (notaAluno >=9 && notaAluno <= 10) 
{
console.log(`Nota do aluno(a) é ${notaAluno} então é A`);
} 
} 





